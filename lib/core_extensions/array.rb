module CoreExtensions
  module Array
    module Finders
      def find id = nil
        self.map { |instance| return instance if (id && instance.respond_to?(:id) && instance.id == id) }
        return nil
      end

      def find_by value, field
        all = []
        self.map { |instance| all.push(instance) if instance.respond_to?(field) && instance.send(field) == value }
        return all
      end
    end
  end
end

# Monkey-patch for Array. Sorry, guys! 
Array.include CoreExtensions::Array::Finders
