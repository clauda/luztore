require 'test_helper.rb'

class Base::Item < Minitest::Test

  def setup
    Base.class_variable_set(:@@records, [Product.new({ id: 4, price: 90 })] )
    @item = Item.new({ order_id: 3, product_id: 4 })
  end

  def test_product
    assert_instance_of Product, @item.product
  end

end