require 'test_helper.rb'

class Base::Order < Minitest::Test

  def setup
    records = [
      Coupon.new({ id: 1, discount: 50.0, type: 'percent', expires_at: '2020/10/01', limit: 1 }),
      Product.new({ id: 1, price: 90 }), 
      Product.new({ id: 2, price: 1290 }),
      Item.new({ order_id: 1, product_id: 1 }), 
      Item.new({ order_id: 1, product_id: 2 })
    ]
    Base.class_variable_set(:@@records, records)
    @order = Order.new({ id: 1, coupon_id: 1 })
    @order.get_items
    @order.items.map { |item| item.send(:set_total) }
  end

  def self.test_closing
    skip
  end

  def test_get_coupon
    assert_instance_of Coupon, @order.coupon
  end

  def test_get_items
    assert_equal 2, @order.items.count
  end
  
  def test_close!
    @order.close!
    assert_equal 690.0, @order.total
  end

end