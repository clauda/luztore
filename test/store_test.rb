require 'test_helper.rb'

class Sales < Minitest::Test

  def setup
    Base.class_variable_set(:@@records, [])
    @store ||= Store::Sales.new %w(coupons.csv orders.csv products.csv order_items.csv test.csv)
  end

  def test_validate!
    assert_equal true, @store.validate!
  end

  def test_magic!
    @store.magic!
    assert_equal 5, @store.coupons.count
    assert_instance_of Product, @store.products.first
    assert_instance_of Coupon, @store.coupons.first
  end

  def test_bill
    assert_equal true, File.exist?('test.csv')
  end

end
