require 'test_helper.rb'

class Base::Coupon < Minitest::Test

  def setup
    @coupon = Coupon.new({ id: 2, discount: 15, type: 'absolute', expires_at: '2020/10/01', limit: 2 })
  end
  
  def test_valid?
    assert_equal true, @coupon.valid?
    @coupon.limit = 0
    assert_equal false, @coupon.valid?
  end

  def test_apply! 
    assert_equal 185, @coupon.apply!(200)
  end

end

