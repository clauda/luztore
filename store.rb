require_relative 'store/base'
require_relative 'store/coupon'
require_relative 'store/product'
require_relative 'store/order'
require_relative 'store/item'

class Store

  # 
  # The Store read coupons, products, orders and items from csv files,
  # calculates the final value for orders.
  # Return results in a csv file.
  # Claudia Farias, 5/5/2016
  # 
  
  DATA_DIR = File.join(Dir.pwd, 'data')
  ACCEPTED_FILES = %w(
    coupons.csv
    products.csv
    orders.csv
    order_items.csv
  )

  class Sales

    attr_reader :coupons, :products, :orders, :items

    def initialize files = []
      @files = ACCEPTED_FILES & files
      validate!

      # Defines output filename.
      @output = files - @files
      @output = @output.any? ? @output.last : 'results.csv'

      @data = Hash.new
    end

    # Shazam
    def magic!
      populate!
      Order.closing
      bill
    end

    def validate!
      if ACCEPTED_FILES != @files
        raise "Invalid args. Some file are missing? Please, try again." 
      else
        true
      end
    end

    private

      # Load files and objects.
      def populate!
        @files.each do |file|
          key = file.split('.')[0]
          @data.merge!({ key => File.join(DATA_DIR, file) })
        end
        ARGV.clear

        @coupons ||= Coupon.fetch(@data['coupons'], [:id, :discount, :type, :expires_at, :limit]) if @data['coupons']
        @products ||= Product.fetch(@data['products'], [:id, :price]) if @data['products']
        @orders ||= Order.fetch(@data['orders'], [:id, :coupon_id]) if @data['orders']
        @items ||= Item.fetch(@data['order_items'], [:order_id, :product_id]) if @data['order_items']
      end

      # Generate .csv output
      def bill
        puts "\nGenerating output file... #{@output}"
        CSV.open(@output, "w") do |csv|
          @orders.each do |order|
            csv << [order.id,order.total]
          end
        end
        puts "Done! File saved in the current path. ;)\n"
      end
  end

end