class Product < Base

  attr_reader :id, :price

  def initialize options = {}
    @id = options[:id].to_i
    @price = options[:price].to_f
  end

end