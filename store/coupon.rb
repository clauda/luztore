class Coupon < Base

  attr_reader :id, :discount, :kind, :expires_at
  attr_accessor :limit

  def initialize options = {}
    @id = options[:id].to_i
    @discount = options[:discount].to_f
    @kind = options[:type]
    @limit = options.fetch(:limit, 0).to_i
    @expires_at = Date.parse(options[:expires_at])
  end

  # Checks coupon expiration, update limit and applys discount to value.
  def apply! value
    if valid?
      puts "Applying coupon. #{@discount}% off."

      if self.kind == 'absolute'
        value -= self.discount.to_f
      else self.kind == 'percent'
        value -= percent_for(value)
      end

      @limit =- 1
    end

    return value
  end

  def valid?
    !expired? and has_limit?
  end

  private

    # Checks if coupoun has expired
    def expired?
      self.expires_at <= Date.today
    end

    # Checks if coupon is out of use
    def has_limit?
      self.limit > 0
    end

    def percent_for value
      value.to_f * self.discount.to_f / 100
    end

end