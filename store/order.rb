class Order < Base

  attr_accessor :total, :items, :coupon, :discount
  attr_reader :id, :coupon_id

  def initialize options = {}
    @id = options[:id].to_i
    @coupon_id = options[:coupon_id].to_i
    @items = []
    @discount = 0.0
    @total = 0.0

    get_coupon
  end

  # Calculate totals
  def close!
    @values = @items.map &:total
    @total = @values.inject(:+)
    apply_discounts!

    # Debugging
    puts "\n\e[32mOrder ##{@id}\e[0m has #{@items.count} items.\n"
    puts "Total \e[33m$ #{@total}\e[0m"
  end

  # Handles discounts. Apply the highest discount.
  def apply_discounts!
    @discount = progressive_discount

    if @coupon && @coupon.valid?
      real_discount = coupon_discount

      if @discount >= real_discount
        apply!
      else
        @total = @coupon.apply!(@total.to_f)
      end
    else
      # Debugging
      # puts "\e[31mSorry, Coupon not found [404] or expired [408]. ;/\e[0m" 
      apply!
    end
  end

  # Close all orders
  def self.closing
    orders = Base.instances_of(self)
    orders.each do |order|
      order.get_items
      order.close!
    end
  end

  # Retrieve order items
  def get_items
    @items = Base.instances_of(Item).find_by(@id, :order_id)
  end

  private

    # Assigns coupon
    def get_coupon
      @coupon = Base.instances_of(Coupon).find(@coupon_id)
    end

    # Apply progressive discount to total.
    def apply!
      # Debugging
      # puts "Applying progressive discount. #{@discount}% off."
      @total -= @total * @discount / 100 if @total
    end

    # Calculate the progressive discount with base on number of items.
    # Start with 10% for orders with 2 items.
    # 5% for each another item. Maximum discount: 40%;
    # return <tt>Float</tt> total discount value 
    def progressive_discount
      discount = 0
      if @items.count >= 2
        discount = (5 * @items.count)
      end
      discount = 40.0 if discount.to_f > 40.0
      return discount
    end

    # Calculate percentage discounted for absolute discounts
    def coupon_discount
      if @coupon.kind == 'absolute'
        return (@coupon.discount / @total.to_f * 100).round
      else
        @coupon.discount
      end
    end

end