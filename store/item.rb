class Item < Base

  attr_reader :order_id, :product
  attr_accessor :total

  def initialize options = {}
    @order_id = options[:order_id].to_i
    @product_id = options[:product_id].to_i
    @total = 0.0
    get_product && set_total
  end

  private

    def get_product
      @product = Base.instances_of(Product).find(@product_id)
    end

    # Updates total value 
    def set_total price = 0.0
      @product ||= get_product
      @total = @product ? @product.price : price
    end

end
