class Base

  @@records ||= []

  def self.fetch file, fields = []
    if File.exist?(file)
      CSV.foreach(file, headers: fields) do |row|
        @@records.push(new(row.to_hash)) unless row[0].nil?
      end
    end
    records = Base.instances_of(self)
    print "#{records.count} #{self.to_s} found.\n"
    return records
  end

  def self.instances_of klass
    @@records.select { |record| record.instance_of?(klass) }
  end

end
