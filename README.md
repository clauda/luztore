### LUZ Store

Ruby 2.3.0

A command line program in Ruby able to calculate the final value
a orders for an store.

`$ bundle install` first.

#### Run

The main file is store.rb. Followed by csv files to load data.

`$ ruby app.rb coupons.csv orders.csv products.csv order_items.csv output.csv`

Files required:
- coupons.csv
    + id,discount,type,expires_at,limit
- orders.csv 
    + id,coupon_id
- products.csv 
    + id,price
- order_items.csv
    + order_id,product_id

(Or just run `$ . run.sh`, if you prefer)

You can provide one more argument to name the output file. 
Default: results.csv 

#### Tests

Run:

`$ rake test` 

or something like

`$ ruby -Ilib:test test/(store|coupon|product|item)_test.rb RACK_ENV=test`
